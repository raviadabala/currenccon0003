import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  username:string;
  password:string;
  degree: boolean;
  ingredients=[

            {name: 'tomato', isChecked:false},
            {name: 'banana',isChecked:true},
            {name: 'mushrooms',isChecked:false},
            {name:'apple',isChecked:false},

];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }


  login(){

   console.log("Username: " +this.username);
    console.log("Password:" +this.password);

   }


   


}
