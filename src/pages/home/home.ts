import { Component } from '@angular/core';
import { NavController,AlertController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { File } from '@ionic-native/file';
import {FileOpener} from '@ionic-native/file-opener';
import {DomSanitizer} from '@angular/platform-browser';
import { Platform } from 'ionic-angular';
import {RegisterPage} from '../register/register';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  private dirs:any;
  photoSelected: boolean;
  photoTaken: boolean;
  clickedImagePath:any;
  respArry : any = [];
  imageURI :string;
  item : any=[{ }];
  Images:any;
  showimg : any = false;
  imgdt:any;
  direArry :any =[];
  filepath:any='MyPicture';
 FileUri='assets/imgs/download (1).png';

options: CameraOptions = {

    quality: 100,
    destinationType: this.camera.DestinationType.DATA_URL,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE,
    cameraDirection:0
  }

  constructor(public navCtrl: NavController,
    private camera: Camera,
    private file: File,
    private fileOpener: FileOpener, 
    private DomSanitizer: DomSanitizer,
    private alertCtrl:AlertController,
    public Platform:  Platform,
  
   
  ) 

 {

 }

  clickImage(){

    this.camera.getPicture(this.options).then((imageData) => {
    
     this.imgdt=imageData;

       let base64Image = 'data:image/jpeg;base64,' + imageData;

      this.clickedImagePath = 'data:image/jpeg;base64,' + imageData;

      var random = Math.random();

      var fname = random+".jpeg";

       this.writeFile(base64Image,"MyPicture",fname); 
        this.SaveFolder(base64Image,"MyPicture/Demo1/Demo2",fname);
      
     
     }, (err) => {
     
     });
  }


       public writeFile(base64Image:any,folderName:string,fileName: any)

           {

          
       let contentType=this.getContentType(base64Image);
       
       let DataBlob = this.base64toBlob(base64Image, contentType); 

       let filePath=this.file.applicationStorageDirectory+'/MyPicture';

       console.log( "directory" +  filePath,fileName, DataBlob, contentType);

       this.file.checkDir(this.file.applicationStorageDirectory, 'MyPicture')

       .then(_ => {

       this.file.writeFile(filePath,fileName, DataBlob, contentType).then((success)=>{

              console.log("File Writed Successfully",success);

       }).catch((err)=>{

         console.log("Error occured while writing File",err);

        })
      }) .catch(err => {
        this.file.createDir(this.file.applicationStorageDirectory, 'MyPicture', false).then(result => {
        this.file.writeFile(filePath,fileName, DataBlob, contentType).then((success)=>{
          // this.getlistdirectory();
                        console.log("File Writed Successfully",success);
          
                 }).catch((err)=>{
          
                   console.log("Error occured while writing File",err);
          
                  })
                })

      });


       }






       public SaveFolder(base64Image:any,folderName:string,fileName: any)
       
                  {
       
                 
              let contentType=this.getContentType(base64Image);
              
              let DataBlob = this.base64toBlob(base64Image, contentType); 
       
              let filePath=this.file.applicationStorageDirectory+'/MyPicture/Demo1/Demo11';
       
              console.log( "directory" +  filePath,fileName, DataBlob, contentType);
       
              this.file.checkDir(this.file.applicationStorageDirectory, 'MyPicture/Demo1/Demo11')
       
              .then(_ => {
       
              this.file.writeFile(filePath,fileName, DataBlob, contentType).then((success)=>{
       
                     console.log("File Writed Successfully",success);
       
              }).catch((err)=>{
       
                console.log("Error occured while writing File",err);
       
               })
             }) .catch(err => {
               this.file.createDir(this.file.applicationStorageDirectory, 'MyPicture/Demo1/Demo11', false).then(result => {
               this.file.writeFile(filePath,fileName, DataBlob, contentType).then((success)=>{
                this. ShowFolders();
                               console.log("File Writed Successfully",success);
                 
                        }).catch((err)=>{
                 
                          console.log("Error occured while writing File",err);
                 
                         })
                       })
       
             });
       
       
              }


         public getContentType(base64Image:any)

          {
              let block = base64Image.split(";");
              let contentType=block[0].split(":")[1];
                return contentType;


         }

         public base64toBlob(b64Data, contentType) {  
          contentType = contentType || '';  
     var  sliceSize = 512;  
          let byteCharacters = atob(b64Data.replace(/^data:image\/(png|jpeg|jpg);base64,/, ''));  
          let byteArrays = [];  
          for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {  
              let slice = byteCharacters.slice(offset, offset + sliceSize);  
              let byteNumbers = new Array(slice.length);  
              for (let i = 0; i < slice.length; i++) {  
                  byteNumbers[i] = slice.charCodeAt(i);  
              }  
              var byteArray = new Uint8Array(byteNumbers);  
              byteArrays.push(byteArray);  
          }  
          let blob = new Blob(byteArrays, {  
              type: contentType  
          });  
          return blob;  
    // var binary_string =  window.atob(b64Data.replace(/^data:image\/(png|jpeg|jpg);base64,/, ''));
    // var len = binary_string.length;
    // var bytes = new Uint8Array( len );
    // for (var i = 0; i < len; i++)  {
    //     bytes[i] = binary_string.charCodeAt(i);
    // }
    // return bytes.buffer;
      }  

  selectFromGallery()
  {

   this.camera.getPicture({

    quality :75,
    destinationType:this.camera.DestinationType.DATA_URL,
    sourceType :this.camera.PictureSourceType.PHOTOLIBRARY,
    allowEdit:true,

    encodingType:this.camera.EncodingType.JPEG,
    targetWidth: 800,
    targetHeight: 800,
    saveToPhotoAlbum:true

   }).then(ImageData =>{


    this.imageURI = 'data:image/jpeg;base64,' +ImageData;
    console.log(this.imageURI);
    console.log(ImageData);
    this.item[0].imageData = ImageData;
    this.photoTaken= false;
    this.photoSelected=true;


   },  error => {
    console.log("ERROR -> " + JSON.stringify(error));
  });
}


getlistdirectory(){
      this.showimg = true;
      this.respArry=[];
      this.direArry=[];
 this.file.listDir(this.file.applicationStorageDirectory, this.filepath).then(resp => {
           console.log("listDir " +JSON.stringify(resp)); 
    
        for(let x=0; x < resp.length; x++){
            this.direArry.push(resp[x]);
        }
 
        console.log("Response Data : "+ JSON.stringify(this.direArry));
        // this.respArry=JSON.stringify(this.respArry);
    });

     }



     Newfolder(fname)

   {
     console.log("filename" +fname);
//     let url = "assets/imgs/images (1).png";
//  console.log("image" + this.imgdt);


      console.log("filepath" +this.filepath);
     
        this.file.checkDir(this.file.applicationStorageDirectory+this.filepath, fname).then(response => {

          console.log('Directory exists'+(response));

        alert('Directory already  exist'+(response));
        })

        .catch(err => {

          console.log('Directory doesn\'t exist'+(err));
          alert('Directory doesn\'t exist'+(err));
          this.file.createDir(this.file.applicationStorageDirectory+this.filepath, fname, false).then(response => {
        

//  console.log("image" + this.imgdt);
            // let base64Image = 'data:image/jpeg;base64,' +   this.imgdt;
            
            // this.clickedImagePath = 'data:image/jpeg;base64,' + this.imgdt;
            // var random = Math.random(); 
            // var fnames = random+".jpeg";
            // this.writeFile(base64Image,fname,fnames);


           (this.file.applicationStorageDirectory+this.filepath, fname)
            console.log('Directory create'+JSON.stringify(response));
            console.log("filename "  +fname);
            console.log("filepath " +this.filepath);
           alert('Directory create'+JSON.stringify(response));
           this.showimg = true;
           this.direArry=[];
           this.respArry=[];
           var path =this.filepath+ fname;
           this.file.listDir(this.file.applicationStorageDirectory, this.filepath).then(resp => {
            console.log("listDir " +JSON.stringify(resp)); 
     
         for(let x=0; x < resp.length; x++){
           this.direArry.push(resp[x]);
         
         }
     
         console.log("Response Data : "+ JSON.stringify(this.direArry));
         // this.respArry=JSON.stringify(this.respArry);
     });
 


           

          })
          .catch(err => {
            console.log('Directory no create'+JSON.stringify(err));
            alert('Directory no create'+JSON.stringify(err));
            
          }); 
        });
      }


      Newcreatefolder(){

        let alert = this.alertCtrl.create({

          title: 'Create Folder!',
          inputs: [
            {
              name: 'FolderName',

              type: 'text',

              placeholder: 'FolderName'

            }

          ],

          buttons: [

            { text: 'Ok', handler:data => this.Newfolder(data.FolderName)},
            
           {text:'Cancel',role:'cancel'}

          ]

        });
alert.present();
      }



      getsubfolders(imgpath='MyPicture',fpath){
        var trimpath=fpath.replace('/data/user/0/io.ionic.starter/','');
        console.log('trimpath :' +trimpath);
        this.filepath=trimpath;
        console.log('oftertrimpath :' +this.filepath);
        console.log("image"+imgpath);
        this.showimg = true;
        this.respArry=[];
        this.direArry=[];
   this.file.listDir(this.file.applicationStorageDirectory,this.filepath).then(resp => {
             console.log("listDir " +JSON.stringify(resp)); 
      
          for(let x=0; x < resp.length; x++){
              // this.respArry.push(resp[x]);
              this.direArry.push(resp[x]);
             
          }
      
          console.log("Response Data : "+ JSON.stringify(this.respArry));
          // this.respArry=JSON.stringify(this.respArry);
      });
  
  
        }




        // nextpage(){

        //  this.navCtrl.push(RegisterPage);



        // }

        ShowFolders(){

       this. getsubfolders('MyPicture','/data/user/0/io.ionic.starter/MyPicture');

    

        }
        





}

